# Kustomize demo

`test-2` is the poc base folder.
It has a `base` folder which represents the basic setup of the component (Simple echo webserver)
It has 2 overlay folders which contain environment specific configurations. 

Example commands to test:

`kubectl kustomize base > base.yaml`: This will generate manifests for the base setup. 
`kubectl kustomize overlay/env-1 > env-1.yaml`: This will generate manifests for the env2 environment
`kubectl kustomize overlay/env-2 > env-2.yaml`: This will generate manifests for the env2 environment